var myApp = angular.module('myApp', []);

myApp.controller('GameLogicController', ['$scope', function ($scope) {
    $scope.winner = null;

    $scope.setRock = function () {
        $scope.userSelection = "Rock";
        $scope.determineWinner($scope.userSelection, $scope.randomizer());
    }

    $scope.setPaper = function() {
        $scope.userSelection = "Paper";
        $scope.determineWinner($scope.userSelection, $scope.randomizer());
    }

    $scope.setScissors = function() {
        $scope.userSelection = "Scissors";
        $scope.determineWinner($scope.userSelection, $scope.randomizer());
    }

    $scope.randomizer = function () {
        var computerOptions = ["Rock", "Paper", "Scissors"];
        return computerOptions[Math.floor(Math.random() * 3)];
    }

    $scope.determineWinner = function (user, computer) {
        if ((user === "Rock" && computer === "Scissors") || (user === "Paper" && computer === "Rock") || (user === "Scissors" && computer === "Paper")) {
            return $scope.winner = "User Wins!";
        }

        if (user === computer)
            return $scope.winner = "Tie!";
        else {
            return $scope.winner = "Computer Wins!";
        }
    }
}]);